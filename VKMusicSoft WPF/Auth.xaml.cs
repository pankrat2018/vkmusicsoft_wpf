﻿using FirstFloor.ModernUI.Windows.Controls;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Navigation;

namespace VKMusicSoft_WPF
{
    /// <summary>
    /// Interaction logic for Auth.xaml
    /// </summary>
    public partial class Auth : ModernWindow
    {
        public Auth()
        {
            InitializeComponent();
        }

        [DllImport("urlmon.dll", CharSet = CharSet.Ansi)]
        private static extern int UrlMkSetSessionOption(int dwOption, string pBuffer, int dwBufferLength, int dwReserved);
        const int URLMON_OPTION_USERAGENT = 0x10000001;


        public void ChangeUserAgent(string Agent)
        {
            UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, Agent, Agent.Length, 0);
        }

        public static string ParseFromURL(string Url, string index)
        {
            Url = Url.Substring(Url.IndexOf(index));
            Url = Url.Replace(index, "");
            return Url;
        }

        private void ModernWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeUserAgent("Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)");
            try
            {
                webBrowser1.Navigate("https://oauth.vk.com/authorize?client_id=3502561&scope=8&redirect_uri=http://oauth.vk.com/blank.html&display=page&response_type=token");
            }
            catch { ModernDialog.ShowMessage("Произошла ошибка проверьте ваше интернет-соединение", "", MessageBoxButton.OK); }
        }

        private void webBrowser1_Loaded(object sender, RoutedEventArgs e)
        {
            dynamic activeX = webBrowser1.GetType().InvokeMember("ActiveXInstance",
                   BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                        null, webBrowser1, new object[] { });
            activeX.Silent = true;
            // Чтобы не вызывало ошибки скриптов
        }

        private void webBrowser1_Navigated(object sender, NavigationEventArgs e)
        {
            if (webBrowser1.Source.ToString().Contains("access_token"))
            {
                string gettkn = ParseFromURL(webBrowser1.Source.ToString(), "access_token=");
                gettkn = gettkn.Remove(gettkn.IndexOf("&"));
                AuthClass.token = gettkn;

                string get_user_id = ParseFromURL(webBrowser1.Source.ToString(), "user_id=");
                AuthClass.userID = get_user_id;
                 Hide();
                MainWindow f = new MainWindow();
                f.ShowDialog();
            }

        }
    }
}
