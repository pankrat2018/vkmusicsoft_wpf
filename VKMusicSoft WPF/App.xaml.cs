﻿using System;
using System.Windows;

namespace VKMusicSoft_WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        App()
        {
            InitializeComponent();
        }

        [STAThread]
        static void Main()
        {
            App app = new App();
            Auth window = new Auth();
            app.Run(window);

        }
    }
}

