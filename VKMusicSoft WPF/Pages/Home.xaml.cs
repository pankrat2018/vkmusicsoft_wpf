﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VKMusicSoft_WPF.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();
        }
        List<string> _links = new List<string>();
        List<string> _artist = new List<string>();
        List<string> _title = new List<string>();

        private readonly MediaPlayer _player = new MediaPlayer();

        int _selectMusic = -1;

        private void buttonAccExit_Click(object sender, RoutedEventArgs e)
        {
            string Path = Environment.GetFolderPath(Environment.SpecialFolder.Cookies);
            try
            {
                Directory.Delete(Path, true);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        private void CheckDownloadFolder()
        {
            if (!Directory.Exists("Downloads"))
            {
                Directory.CreateDirectory("Downloads");
            }
        }

        private void DownloadSong(object fileName)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(fileName.ToString().Split('•')[0], "Downloads\\" + fileName.ToString().Split('•')[1] + ".mp3");
                }
            }
            catch { }
        }

      private void DownloadAllSongs(object id)
        {
           // bool downloaded = false;
           // while(!downloaded)
           // {
                try
                {

                using (WebClient webClient = new WebClient())
                {
                    int index = int.Parse(id.ToString());
                    webClient.DownloadFile(_links[index], "Downloads\\" + _artist[index] + " — " + _title[index] + ".mp3");
                }
                  
                }
                catch { /*downloaded = false;*/ }
            //}

        }

        private void PrintSongs()
        {
            Dispatcher.BeginInvoke(new Action(delegate
            {
                for (int i = 0; i < _links.Count; i++)
                {
                    listBoxMusic.Items.Add(_artist[i] + " — " + _title[i]);
                }

            }));
        }

        private void GetMusic()
        {
            //  _links.Clear();
            // _artist.Clear();
            // _title.Clear();
            VK api = new VK();
            api.GetMusicList(ref _links, ref _artist, ref _title);
            PrintSongs();
            Dispatcher.BeginInvoke(new Action(delegate
            {
               listBoxMusic.SelectedIndex = 0;
            }));

            _selectMusic = 0;

        }
        private void PlayMusic()
        {
            int indexMusic = ParseIndex();
            if (indexMusic != -1)
            {
                _player.Open(new Uri(_links[indexMusic], UriKind.RelativeOrAbsolute));
                _player.Play();
                btnPlay.Visibility = Visibility.Hidden;
                btnStop.Visibility = Visibility.Visible;
                _selectMusic = indexMusic;
            }
        }

        private void StopMusic()
        {
            _player.Stop();
            btnStop.Visibility = Visibility.Hidden;
            btnPlay.Visibility = Visibility.Visible;
        }

        private void NextMusic()
        {
            _player.Stop();

            int indexMusic = ParseIndex();
            if (indexMusic != -1)
            {
                _player.Open(new Uri(_links[++indexMusic], UriKind.RelativeOrAbsolute));
                ++listBoxMusic.SelectedIndex;
                _player.Play();
            }

        }

        private void PreviousMusic()
        {
            _player.Stop();

            int indexMusic = ParseIndex();
            if (indexMusic != -1)
            {
                _player.Open(new Uri(_links[--indexMusic], UriKind.RelativeOrAbsolute));
                --listBoxMusic.SelectedIndex;
                _player.Play();
            }
            

        }

        private int ParseIndex()
        {
            try
            {
                int toDown = 0;
                for (int i = 0; i < _title.Count; i++)
                {
                    if (_artist[i] + " — " + _title[i] == listBoxMusic.Items[listBoxMusic.SelectedIndex].ToString())
                    {
                        toDown = i;
                    }
                }
                return toDown;
            }
            catch { ModernDialog.ShowMessage("Выберите песню для проигрывания", "", MessageBoxButton.OK); return -1;  }
        }

        private void UserControl_Initialized(object sender, EventArgs e)
        {
            new Thread(GetMusic).Start();
        }

        private void listBoxMusic_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                int index = ParseIndex();

                if (ModernDialog.ShowMessage("Скачать " + _artist[index] + " — " + _title[index] + "?", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    CheckDownloadFolder();
                    new Thread(DownloadSong).Start(_links[index] + "•" + _artist[index] + " — " + _title[index]); // точка - разделитель, чтобы была возможность разделить на названия
                }
            }
            catch { ModernDialog.ShowMessage("Произошла ошибка, возможно вы не выделили желаемую песню", "", MessageBoxButton.OK); }
        }

        private void buttonDownloadAll_Click(object sender, RoutedEventArgs e)
        {
            CheckDownloadFolder();
            for (int i = 0; i < _links.Count; i++)
                ThreadPool.QueueUserWorkItem(DownloadAllSongs, i);
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            PlayMusic();
            
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            StopMusic();
        }

        private void listBoxMusic_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            PlayMusic();

        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            NextMusic();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            PreviousMusic();
            
        }

        private void sliderVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _player.Volume = sliderVolume.Value;
        }

        private void listBoxMusic_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void textBoxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            listBoxMusic.Items.Clear();
            for (int i = 0; i < _links.Count; i++)
            {
                if (_artist[i].ToLower().Contains(textBoxSearch.Text.ToLower()) || _title[i].ToLower().Contains(textBoxSearch.Text.ToLower()))
                {
                    listBoxMusic.Items.Add(_artist[i] + " — " + _title[i]);
                }
            }


            if (textBoxSearch.Text == "")
            {
                PrintSongs();

                if (_selectMusic != -1)
                {
                    listBoxMusic.SelectedIndex = _selectMusic;
                    listBoxMusic.ScrollIntoView(listBoxMusic.SelectedIndex);
                }
            }
        }
    }
}
