﻿namespace VKMusicSoft_WPF
{
  static class AuthClass
    {
        private static string _token = "";
        public static string token
        {
            get
            {
                //Clipboard.SetText(_token);
                return _token;
            }
            set
            {
               // Clipboard.SetText(_token);
                _token = value;
            }
        }

        private static string _userID = "";
        public static string userID
        {
            get
            {
                return _userID;
            }
            set
            {
                _userID = value;
            }
        }
    }
}
