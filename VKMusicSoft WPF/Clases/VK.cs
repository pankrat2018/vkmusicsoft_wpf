﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml;

namespace VKMusicSoft_WPF
{
    class VK
    {
        public void GetMusicList(ref List<string> links, ref List<string> artist, ref List<string> title)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("https://api.vk.com/method/audio.get.xml?uid=" + AuthClass.userID + "&access_token=" + AuthClass.token);
               // System.Diagnostics.Process.Start("https://api.vk.com/method/audio.get.xml?uid=" + AuthClass.userID + "&access_token=" + AuthClass.token);
                XmlNodeList urls = doc.SelectNodes("response/audio/url");
                foreach (XmlNode url in urls)
                {
                    links.Add(url.InnerText.Replace("&amp;", ""));
                }
                XmlNodeList artists = doc.SelectNodes("response/audio/artist");
                foreach (XmlNode _artist in artists)
                {
                    artist.Add(_artist.InnerText.Replace("&amp;", ""));
                }
                XmlNodeList titles = doc.SelectNodes("response/audio/title");
                foreach (XmlNode _title in titles)
                {
                    title.Add(_title.InnerText.Replace("&amp;", ""));
                }


            }
            catch (Exception ex) { ModernDialog.ShowMessage(ex.Message, "", MessageBoxButton.OK); }
        }

        public void GetGlobalMusicList(string musicName,ref List<string> links, ref List<string> artist, ref List<string> title)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("https://api.vk.com/method/audio.search.xml?access_token=" + AuthClass.token + "&q=" + musicName);

                // System.Diagnostics.Process.Start("https://api.vk.com/method/audio.search.xml?access_token=" + AuthClass.token + "&q=" + musicName);
                XmlNodeList urls = doc.SelectNodes("response/audio/url");
                foreach (XmlNode url in urls)
                {
                    links.Add(url.InnerText.Replace("&amp;", ""));
                }
                XmlNodeList artists = doc.SelectNodes("response/audio/artist");
                foreach (XmlNode _artist in artists)
                {
                    artist.Add(_artist.InnerText.Replace("&amp;", ""));
                }
                XmlNodeList titles = doc.SelectNodes("response/audio/title");
                foreach (XmlNode _title in titles)
                {
                    title.Add(_title.InnerText.Replace("&amp;", ""));
                }


            }
            catch (Exception ex) { ModernDialog.ShowMessage(ex.Message, "", MessageBoxButton.OK); }
        }
    }
}
